import pylab
import numpy as np
#Thomas Leahy

def Vprime_bistable(x):
    """
    Derivative of the V function
    """
    return x**3-x

def V_bistable(x):
    """
    The V function for the bistale programme
    """
    return (x**4)/4-(x**2)/2

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
    return np.array(xvals), np.array(tvals)





def montecarlo(f, X, title):
    """
    Function to compute the Monte Carlo estimate of the expectation
    E[f(X)], with N samples

    inputs:
    f - a Python function that applies a chosen mathematical function to
    each entry in a numpy array
    X - a Python function Nthat takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    N - the number of samples to use
    title = string containing plot title
    """

    data = f(X)
    theta=np.cumsum(data)/np.arange(1,data.size+1)

    pylab.figure()
    pylab.plot(np.arange(1,data.size+1), theta)
    pylab.title(title)
    pylab.xlabel('N')
    pylab.ylabel('Expected Value')

def error_plot(f, X, true_sol, r, title):
    """
    Function that takes at inputs:
    f - a Python function that applies a chosen mathematical function to
    each entry in a numpy array
    X - a Python function Nthat takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    true_sol - the true solution
    r - adjustment factor for comparison
    title - title of the plot
    output: error plot with comparison
    """
    data = f(X)
    theta=np.cumsum(data)/np.arange(1,data.size+1)

    pylab.figure()
    pylab.loglog(np.arange(1,data.size+1), abs(theta - true_sol), label='Error')
    pylab.loglog(np.arange(1,data.size+1), (r/np.arange(1,data.size+1)**0.5), label= '$r/\sqrt{N}$') 
    pylab.title(title)
    pylab.xlabel('N')
    pylab.ylabel('Error')
    pylab.legend(loc='best')


def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**2

def linear(x):
    """
    Function to compute the identity of the input. If the input is 
    a numpy array, this returns a numpy array of all of the linear values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the identity of all of the values
    """
    return x

def invarient_sol(V, c, x, sigma):
    """
    Function that returns the invarient solution
    """
    return c*np.exp(-(2)/(sigma**2)*(V(x)))


def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)


if __name__ == '__main__':
#Exercise 1
    dt = 0.1
    sigma =0.5
    T = 1000.0
    n= 500
    
    x,t = brownian_dynamics(0.0,Vprime_bistable,sigma,dt,T,dt)
    pylab.plot(t,x)
    pylab.xlabel('N range')
    pylab.ylabel('Expected Value')

    pylab.show()



#Exercise 2

    histogram_array = np.ones((n,T/dt+1))
    
    for row in range(n):
        x,t = brownian_dynamics(1.0,Vprime_bistable,sigma,dt,T,dt)

        histogram_array[row,:] = x

    #creating a subplot of histograms
    f, (ax1, ax2, ax3, ax4, ax5, ax6, ax7) = pylab.subplots(7, sharex=True, sharey=True)


    ax1.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax1.hist(histogram_array[:,1], bins=100, normed = True)
    ax1.set_title('Histogram of Values vs. Invarient Solution')

    ax2.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax2.hist(histogram_array[:,25], bins=100, normed = True)

    ax3.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax3.hist(histogram_array[:,100], bins=100, normed = True)

    ax4.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax4.hist(histogram_array[:,150], bins=100, normed = True)

    ax5.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax5.hist(histogram_array[:,200], bins=100, normed = True)

    ax6.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax6.hist(histogram_array[:,500], bins=100, normed = True)

    ax7.plot(np.linspace(-1.5, 1.5, 800), invarient_sol(V_bistable, 0.1, np.linspace(-1.5, 1.5, 800), sigma), '.')
    ax7.hist(histogram_array[:,1000], bins=100, normed = True)

    print('At the inital plot at T=1 we see that the distribution is clustered around the one point, but as time increases we see that the distribution spreads and we begin to see a bimodol structure forming. At time equal to 20 we seem to get a fairly good approximation, at time equal to 50 the approximation is a lot better. Of course when T=100 the distribution is very accurate compared to the analyical solution. So it converges relatively quickly. Note. Please make the histogram image large on your screen to see it properly.')
    f.subplots_adjust(hspace=0)
    pylab.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
    
    pylab.show()


#Exercise 3
    x,t = brownian_dynamics(1.0,Vprime_bistable,0.5,0.1,1000000,0.1)

    montecarlo(linear,x, "Linear")

    montecarlo(square, x, "Square")

    error_plot(linear, x, 0, 1,"Error plot for Linear")

    error_plot(square, x, 0.85, 0.4, "Error plot for Square")
    print('r is some adjusting constant')
    print(' We see that the convergence rate for Linear is roughly 1/sqrt(N). We can see this by comparing it to the line also plotted. For Square we see that the convergence is 0.4/sqrt(N), so it has a slightly slower convergence rate than we had for Linear.')
    pylab.show()

