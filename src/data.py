from scipy import integrate
import numpy as np
import pylab

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""
def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """

    arr=[sigma*(x[1] - x[0]), x[0]*(rho - x[2])-x[1], x[0]*x[1]-beta*x[2]]    

    return arr

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10., 28., (8./3)))
    return data, t

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()
            
if __name__ == '__main__':
    mydat, mytime = getdata([-0.587,-0.563 ,16.870 ],100.0,0.05)
    mydat1, mytime1 = getdata(np.array([-0.587,-0.563 ,16.870 ])*1.01,100.0,0.05)
#    plot3D(mydat[:,0], mydat[:,1], mydat[:,2])
#    plot3D(mydat1[:,0], mydat1[:,1], mydat1[:,2])
#    pylab.plot(mytime1,mydat1)
#    pylab.plot(mytime,mydat)
    dist_in = mydat - mydat1
    dist_new = (dist_in[:,0]**2 + dist_in[:,1]**2 + dist_in[:,2]**2)**0.5
    pylab.semilogy(mytime, dist_new)
    pylab.xlabel('time')
    pylab.ylabel('log')
    pylab.show()



