import numpy as np
import pylab
from scipy import integrate

def linear_lorenz_vectorfield(y,t,sigma,rho,beta):
    """
    Function to integrate the lorenz equations together
    with the linearised equations about that trajectory.
    
    inputs:
    y - array containing (x,y,z,dx,dy,dz)
    t - time, not used but needed for the interface
    rho, sigma,beta - parameters
    """

    x = y[:3]
    dx = y[3:]

    #This needs changing
    A = np.array([[-sigma, sigma, 0], [(rho-x[2]), -1, -x[0]], [x[1], x[0], -beta]]) #martirx from part 2
    xdot = np.array([sigma*(x[1]-x[0]),x[0]*(rho-x[2])-x[1], x[0]*x[1] - beta*x[2]]) #the lorenz xdot equation (x[0]=x)
    dxdot = np.dot(A, dx)

    #combine xdot and dxdot into one field
    return np.concatenate((xdot,dxdot))

def Phi(x,dx,T):
    """
    Function to integrate the linearised Lorenz equations about
    a given trajectory.
    Inputs:
    x - the initial conditions for the Lorenz trajectory
    dx - the initial conditions for the perturbation
    T - the length of time to integrate for

    Outputs:
    dx - the final value of the perturbation
    """

    y0 = np.concatenate((x,dx))
    t = np.array([0.,T])
    data = integrate.odeint(linear_lorenz_vectorfield, y0, t=t, args=(10.,28.,8./3.))
    
    return data[1,3:]

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()

if __name__ == '__main__':
    mydat, mytime = Phi([-0.587,-0.563,16.870],[1,1],100)
    plot3D(mydat[:,0],mydat[:,1],mydat[:,2])



