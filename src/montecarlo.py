import numpy as np
import pylab

def square(x):
   """
   Function to compute the square of the input. If the input is
   a numpy array, this returns a numpy array of all of the squared values.
   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the square of all of the values
   """
   return x**2

def normal(N):
   """
   Function to return a numpy array containing N samples from
   a N(0,1) distribution.
   """
   return np.random.randn(N)

def montecarlo(f, X, N):
    """
    Function to compute the Monte Carlo estimate of the expectation
    E[f(X)], with N samples

    inputs:
    f - a Python function that applies a chosen mathematical function to
    each entry in a numpy array
    X - a Python function Nthat takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    N - the number of samples to use
    """

    theta = np.sum(f(X(N)))/N
    error = abs(1-theta)
    sigma = (1/(N-1))*np.sum(np.square((np.array(X(N)) - theta)))
    error_estimate = 2*np.sqrt((sigma/N))
    return theta, error_estimate, error

def f_sin(x):
    return np.sqrt(x**2 + 1) + np.sin(x)


if __name__ == '__main__':    
    

    ep = []
    est_error = []
    n_range = np.logspace(1,7,num=5)
    for n in n_range:
        theta, est_err, err = montecarlo(f_sin, normal, n)
        ep.append(err)
        est_error.append(est_err)

    print theta
    print err
    print est_err
    
    pylab.loglog(n_range, est_error, label = "Estimated error")
    pylab.loglog(n_range, n_range**-0.5, label = "$1/\sqrt{N}$")
    pylab.loglog(n_range, ep, label = "Actual Error")
    pylab.legend()
    pylab.xlabel('N')
    pylab.ylabel('Error')
    pylab.show()
