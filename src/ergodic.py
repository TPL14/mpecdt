import numpy as np
import pylab
from scipy import integrate
from data import getdata
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

def ensemble(M,T, v=0.1, sigma=10.0, rho=28., beta=8./3.):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = v*np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(sigma,rho,beta),
                                mxstep=100000)

        ens[m,:] = data[1,:]

    return ens

def f(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension ensemble index/time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2

def g(x):
    """
    Alternetive function
    """

    return d[:,0]**0.5 + d[:,1]**0.5 + d[:,2]**0.5





def sp_average(f, M, T, v):

    """
    Function to calculate the spatial average
    """

    
    X = ensemble(M,T, v)
    return np.average(f(X))


def t_average(M=1):
    """
    Function to calculate the time average
    """
    
    tset = []
    delta = 0.1
    t_range = np.arange(0.1,30,delta)
    for t in t_range:
        #get orbits for invariant measure
        mydat , mytime = getdata(np.random.randn(3),t,delta)
        f_X=f(mydat)
        n_t=len(f_X) #length of f_X
        t_average = (1./n_t)*np.sum(f_X)
        tset.append(t_average) #adding to the list
    return tset

def ensemble_lim(f, ens, T , Deltat, sigma=10.0, rho=28., beta=8./3.):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """

    t = np.arange(0.,T,Deltat)
    mydat = integrate.odeint(vectorfield, 
                        ens, t=t, args=(sigma,rho,beta))
    f_x = f(mydat)
    time_f = np.average(f_x)

    return time_f

def getdata(y0,T,Deltat, rho = 28., sigma= 10., beta = 8./3):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(sigma, rho, beta))
    return data, t

def fin_dif_rho(f, ep):
    """
    Finite difference for rho
    """
    f_1 = ensemble_lim(f,0.1*np.random.randn(3), 200 , 0.05, rho=28.+ep)
    f_2 = ensemble_lim(f,0.1*np.random.randn(3), 200, 0.05)

    return (f_1 - f_2)/ep


def fin_dif_sigma(f, ep):
    """
    Finite difference for sigma
    """
    f_1 = ensemble_lim(f,0.1*np.random.randn(3), 200 , 0.05, sigma=10+ep)
    f_2 = ensemble_lim(f,0.1*np.random.randn(3), 200, 0.05)

    return (f_1 - f_2)/ep

def fin_dif_beta(f,ep):
    """
    Finite difference for beta
    """
    f_1 = ensemble_lim(f,0.1*np.random.randn(3), 200 , 0.05, beta=8./3+ep)
    f_2 = ensemble_lim(f,0.1*np.random.randn(3), 200, 0.05)

    return (f_1 - f_2)/ep


if __name__ == '__main__':

    Ens = ensemble(100, 0.5)
    ensemble_plot(Ens,'T=0.5')
    pylab.show()

############
#Exercise 2#############################################################################
############

#print the variance 

    av = []
    for i in range(10): #run the follwing 10 times
        av.append(sp_average(f, 100,1, 0.1))
        var = np.var(av)

    print('The following are the average elements',av)
    print(var,'is the variance of the element of av.')


#investigate the change in spatial average with change in variance of initial condition


    
    var_o=[]
    v_range= np.arange(0.1,1,0.1)
    for v in v_range:
        var = []
        for i in range(20):
           var.append(sp_average(f, 100,1, v))

        var_o.append(np.var(var))
    pylab.plot(v_range, var_o)
    pylab.xlabel('standard deivation')
    pylab.ylabel('spatial average variance')
    pylab.show()

#part b################################################################################


    
    t_range = np.arange(0.1,30,0.1)
    pylab.figure()
    for l in range(5):
        pylab.plot(np.arange(1,np.size(t_range)+1), t_average( M=1))
    pylab.xlabel('Number of iterations')
    pylab.ylabel('Time Average')

    print('The rate of convergence is very quick even after just a few time steps')
    print('The independence is demonstrated by taking random initial condtions and we see that they all converge to roughly the same quantity.')
    pylab.show()







############
#Exercise 3###########################################################################
############


    for rho in [14, 18, 22, 28, 34]: #range of values to see the difference
        fig = pylab.figure()
        mydat, mytime = getdata(0.1*np.random.randn(3), 500, 0.1, rho=rho)
        ax = fig.add_subplot((111), projection='3d')
        ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'k-')
        pylab.title('rho = %f' % rho)

    pylab.show() #plot just for rho

    for sigma in [4, 8, 10, 14, 18]: #range of values to see the difference
        fig = pylab.figure()
        mydat, mytime = getdata(0.1*np.random.randn(3), 500, 0.1, sigma=sigma)
        ax = fig.add_subplot((111), projection='3d')
        ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'k-')
        pylab.title('sigma = %f' % sigma)
        
    
        
    pylab.show() #plot just for sigma

    for beta in [1.2, 1.6, 2.2, 8./3, 3, 3.4]: #range of values to see the difference
        fig = pylab.figure()
        mydat, mytime = getdata(0.1*np.random.randn(3), 500, 0.1, beta=beta)
        ax = fig.add_subplot((111), projection='3d')
        ax.plot(mydat[:,0], mydat[:,1], mydat[:,2], 'k-')
        pylab.title('beta = %f' % beta)
    
        
  
    print('We can see from the plots that Rho is the parameter that the attractor is most sensitive to as the shape of the attractor becomes distorted. Although the shape of the attractor is sensitive to sigma and beta also but not to the same extent. The trajectories of sigma and beta seem to keep somewhat the same structure as before.')
    pylab.show() #plot for beta

#part b############################################################################

#rho
#smaller range of value
    r_range = np.arange(27., 29., 0.05)
    init_con = 0.1*np.random.randn(3)
    time_array = []
    for r in r_range:
        time_array.append(ensemble_lim(f,init_con, 800, 0.05, rho = r))

    pylab.plot(r_range, time_array)
    pylab.ylabel('$E[f(X)]$')
    pylab.xlabel('$rho$')
    pylab.show()

#larger range
    r_range = np.arange(20.,29., 0.1)
    init_con = 0.1*np.random.randn(3)
    time_array = []
    for r in r_range:
        time_array.append(ensemble_lim(f,init_con, 800, 0.05, rho = r))


    pylab.plot(r_range, time_array)
    pylab.ylabel('$E[f(X)]$')
    pylab.xlabel('$rho$')
    print('When the value of rho is above and below 24 we see that we get a linear response, however at 24 there seems to be a discontinuity.')
    pylab.show()

#beta

    b_range = np.arange(2., 3.5, 0.05)
    init_con = 0.1*np.random.randn(3)
    time_array = []
    for b in b_range:
        time_array.append(ensemble_lim(f,init_con, 800, 0.05, beta = b))


    pylab.plot(b_range, time_array)
    pylab.ylabel('$E[f(X)]$')
    pylab.xlabel('$beta$')
    print('For beta we a see any region somewhat linear response with a large jump at 3.4.')
    pylab.show()

#sigma

    s_range = np.arange(9.95, 15, 0.05)
    init_con = 0.1*np.random.randn(3)
    time_array = []
    for s in s_range:
        time_array.append(ensemble_lim(f,init_con, 800, 0.05, sigma = s))


    pylab.plot(s_range, time_array)
    pylab.ylabel('$E[f(X)]$')
    pylab.xlabel('$sigma$')
    print('Again we see no linear response for sigma. However its says relative close to a value of around 775.')
    pylab.show()



#######FINITE DIFFERENCE###########################################################
    ep_range = np.arange(0.01,1.5,0.01)
#Finite difference for Rho
    ep_vec = []
    for ep in ep_range:
        ep_vec.append(fin_dif_rho(f, ep))


    pylab.plot(ep_range, ep_vec)
    pylab.xlabel('Change in epsilon')
    pylab.ylabel('Value of partial derivative of the expectation')
    pylab.title('Rho')
    print('Rho appears to be the most sensitive as the value of the partial derivative seems to converge to the constant positve value. Where as in the next plots we will see that beta and sigma converge to a partial equal to 0.')
    pylab.show()




#Finite difference for Beta
    ep_vec = []
    for ep in ep_range:
        ep_vec.append(fin_dif_beta(f,ep))


    pylab.plot(ep_range, ep_vec)
    pylab.xlabel('Change in epsilon')
    pylab.ylabel('Value of partial derivative of the expectation')
    pylab.title('Beta')
    pylab.show()


#Finite difference for Sigma
    ep_vec = []
    for ep in ep_range:
        ep_vec.append(fin_dif_sigma(f, ep))


    pylab.plot(ep_range, ep_vec)
    pylab.xlabel('Change in epsilon')
    pylab.ylabel('Value of partial derivative of the expectation')
    pylab.title('Sigma')
    print('The response for rho we discussed earlier, there is a point at 24 where there is a discontinuity an dhence not differentiable. The rest of the function for rho is smooth(ish). For Beta there seems to be a big jump at 3.4 and so it is not linear and not differentiable at this point. The rest of Beta is relatively smooth and linear. Sigma is not linear and not differentiable anywhere.')
    pylab.show()



