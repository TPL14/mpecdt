import numpy as np
import pylab as pl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy import integrate

def myprior():
    """
    Function to draw a sample from a prior distribution given by the
    normal distribution with variance alpha.
    outputs: a random sample
    """
    alpha = 0.5
    return alpha*np.random.randn(3) + [-0.5, -0.55, 17.0]


def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    sigma - lorenz parameter
    rho - lorenz parameter
    beta - lorenz parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)

def getdata(y0,T=1,Deltat=0.1, sigma = 10., rho =28., beta = 8./3):
    """
    Inputs:
    y0 - the initial condition
    T - final time
    Deltat - time intervals 
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(sigma, rho, beta))
    return data



def Phi(f,x,y, sigma):
    """
    inputs:
    f -  forward model
    x - initial x
    y - observation
    sigma - standard dev
    """
    #np.linalg.norm is a function that returns the appropriate norm given the input array
    return 0.5*np.linalg.norm(f(x)[-1,:]-y)**2/sigma**2


def mcmc(f,myprior,x0,yhat,N,beta, sigma):
    """
    Function to implement the MCMC pCN algorithm.
    inputs:
    f - the function implementing the forward model
    prior_sampler - a function that generates samples from the prior
    x0 - the initial condition for the Markov chain
    yhat - the observed data
    N - the number of samples in the chain
    beta - the pCN parameter
    sigma: sigma^2 is the noise variance

    outputs:
    xvals - the array of sample values
    avals - the array of acceptance probabilities
    """
    xvals = np.zeros([N+1,3])
    xvals[0,:] = x0
    avals = np.zeros([N]) 

    for i in range(N):
        w = myprior()
        v = np.sqrt(1 - beta**2) * xvals[i,:] + beta*w

        avals[i] = min(1., np.exp(Phi(f, xvals[i,:], yhat, sigma) - Phi(f, v, yhat, sigma)))
        
        u = np.random.uniform()

        if u < avals[i]:
            xvals[i+1,:] = v
        else: 
            xvals[i+1,:] = xvals[i,:]

    return xvals,avals

if __name__ == '__main__':

    T=1
    mydat = getdata([-0.587, -0.563, 16.87]) 
    plot3D(mydat[:,0],mydat[:,1],mydat[:,2])
    pl.show()

    yhat = mydat[-1,:] + 0.5*np.random.randn(3)

    beta = 0.001

    xvals,avals = mcmc(getdata,myprior,[-0.587,-0.563,16.870],yhat,10000,beta, 0.5)

    average_a = np.cumsum(avals)/np.arange(1,avals.size+1)
    pl.plot(np.arange(1,avals.size+1),average_a)
    pl.title("Average of a")
    pl.xlabel('N')
    pl.ylabel('Acceptance proabaility')
    pl.savefig('acceptance.pdf')
    #print last value
    print (average_a[-1])
    pl.show()

    burnin = 3000
    burnin_x = xvals[burnin:,:]

    print ('Mean is X:', np.mean(burnin_x[:,0]), 'Mean is Y:', np.mean(burnin_x[:,1]), 'Mean is Z:', np.mean(burnin_x[:,2]))

    x=burnin_x[:,0]
    y=burnin_x[:,1]
    z=burnin_x[:,2]
    X=np.vstack((x,y,z))
    print ('The covariance matrix is \n', np.cov(X))
    print('See README.txt')

    pl.figure()
    pl.title('X')
    pl.hist(burnin_x[:,0], bins=20, normed=1)
    
    pl.figure()
    pl.title('Y')
    pl.hist(burnin_x[:,1], bins=20, normed=1)

    pl.figure()
    pl.title('Z')
    pl.hist(burnin_x[:,2], bins=20, normed=1)

    pl.show()


