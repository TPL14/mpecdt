import numpy as np
import pylab

def a(x,t,c):
    return c*x

def b(x,t,e):
    return e

def f(x,t,c):
    return c*x

def g(x,t,e):
    return e*x



def euler_maruyama(x,a,b,dt,T,tdump, c=-1.0, e=1.0):
    """
    Solve the stochastic differential equation

    dx = a(x,t)dt + b(x,t)dW

    using the Euler-Maruyama method

    x^{n+1} = x^n = a(x^n,t^n)dt + b(x^n,t^n)dW

    inputs:
    x - initial condition for x
    a - a function that takes in x,t and returns a(x,t)
    b - a function that takes in x,t and returns b(x,t)
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    Wvals - a numpy array containing the values of W at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    Wvals = [0]
    W = 0.
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += dt*a(x,t,c) + b(x,t,e)*dW

        W += dW
        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
            Wvals.append(W)
    return np.array(xvals), np.array(tvals), np.array(Wvals)
#Q1
#x,t,W = euler_maruyama(1.0,a,b,dt=0.001,T=10.0,tdump=0.01,c=-1.0, e=1.0)
#pylab.plot(t,x)
#pylab.plot(t,W)
#pylab.xlabel('t')
#pylab.ylabel('x')
#pylab.show()

#Q2
c = -2.0
e = 3.0
x,t,W = euler_maruyama(1.0,f,g,dt=0.001,T=10.0,tdump=0.01,c=c, e=e)
exact = 1.0*np.exp((c-0.5*e**2)*t +e*W)
pylab.plot(t,x)
pylab.plot(t,exact)
pylab.xlabel('x')
pylab.ylabel('W')
pylab.show()




