import numpy as np
import pylab

"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**2

def linear(x):
    """
    Function to compute the identity of the input. If the input is 
    a numpy array, this returns a numpy array of all of the linear values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the identity of all of the values
    """
    return x

def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    y = np.ones(x.shape)
    y[x<0] = 0.0
    y[x>1] = 0.0
    return y

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    
    Y = Y(N)
    ratio = (rho(Y)/rhoprime(Y))
    

    theta = (1/np.sum(ratio))*(np.sum(f(Y)*(ratio)))

    return theta



def samp_clus(n):
    """
    Function to transform the uniform_pdf to a clustered pdf input n
    """
    u = uniform(n)
    x = -0.1*np.log(1 - u + u*np.exp(-10))
    return x

def new_exp(x):
    """
    Function to return the mathematical function to be computed.
    """
    return np.exp(-10*x)*np.cos(x)


def uniform(N):
    """
    Function to return a numpy array containing N samples from 
    a Uniform[0,1] distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.rand(N)



def cluster_pdf(x):
    """
    density function for the new clustered 
    """
    trans = np.zeros(np.size(x))
    trans[x>0] =(10*np.exp(-10*x))/(1-np.exp(-10))
    trans[x>1] =0
    return trans



if __name__ == '__main__':
#Exercise 1
    theta = importance(linear,normal, uniform_pdf,normal_pdf,1000000)
    print("theta = ", theta)
    print('Note: I am running my plots for a large number of data points.')
    
    N_range = np.arange(10, 100000,50)
    error = np.zeros(np.size(N_range))
    for i, n in enumerate(N_range):
        error[i]=(np.abs(importance(linear, normal,  uniform_pdf,normal_pdf, n)-1./2))
#plotting
    pylab.loglog(N_range,error, 'b', label='Error')
    pylab.loglog(N_range, 0.7*N_range**(-0.5), 'r', label='N^(-0.5)')
    pylab.xlabel('N')
    pylab.ylabel('Error of importance sample')
    pylab.title('Importance sample error vs. N')
    pylab.legend(loc='best')
    print('We can see from the plot that the error of the estimate decreases to a very small value as time increases.')
    
    pylab.show()

#Exercise 2
    theta_clus = importance(new_exp,samp_clus,uniform_pdf,cluster_pdf,1000000)
    print("theta_cluster = ", theta_clus)
    
    sample = np.zeros(np.size(N_range))
    exact_sol = (10/101)- ((10*np.cos(1) - np.sin(1))/(101*np.exp(10)))
    for i,n in enumerate(N_range):
        sample[i] = np.abs(importance(new_exp,samp_clus,uniform_pdf,cluster_pdf,n) - exact_sol)
#plotting
    pylab.loglog(N_range,sample, 'r', label='Error')
    pylab.loglog(N_range, 0.7*N_range**(-0.5), 'g', label='N^(-0.5)')
    pylab.xlabel('N')
    pylab.ylabel('Error of importance sample')
    pylab.title('Importance (clustered distribution) sample error vs. N')
    pylab.legend(loc='best')    
    print('Again, we can see from the plot that the error of the estimate decreases to a very small value as time increases, as expected.')

    pylab.show() 

