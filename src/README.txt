Hi Colin!

Files to view:

    lorenz-mcmc_TPL.py
    acceptance.pdf
    this README text file!


The lorenz-mcmc_TPL.py file contains the code that should implement a forward model function based on the Lorenz model
and use the pCN algorithm to obtain samples from the posterior distribution. The code has been adapted from the mcmc.py
file to accept vector entries, here namely the 3 dimensional vector (x,y,z).

When the code is run it will produce, a trajectory of the Lorenz model after one time step, then once this image is closed 
down, the code will then produce the average acceptance plot (also see acceptance.pdf), once this plot is closed down, the 
code should then return histogram plots of the distribution for each of the components for (x,y,z), note that they are 
all centred at incorrect values as I could not find a correct combination of variables as a result the acceptance rate is 
not 0.25. The code will also return in the terminal the mean of each component and the resulting covariance matrix.

We see that the average value of a is converging to zero, this is that the average acceptance rate is going to zero,
so in the limit all sample are getting rejected by the pCN algorithm (this is obviously not ideal). The histogram plots do 
not look that normally distributed, however if you chop of the far tails of the plots they do look somewhat normally distributed.

If we could find a correct combination of parameters, we would expect that these histograms to be centred around the 
initial condition that we gave in myprior, with some noise.
