Computational Lab: Dynamics 1
=============================

This is the tutorial material for Computational Lab 1 in the Dynamics
core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpcdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about data-driven forecasting, and is based
on parts of Chapter 1 of the book "Probabilistic forecasting and
Bayesian data assimilation" by Reich and Cotter (CUP, in press).

We are going to look at forecasting solutions of the Lorenz system

.. math::

   \begin{eqnarray}
   \dot{x} &= \sigma (y-x)\\
   \dot{y} &= x(\rho-z) - y  \\ 
   \dot{z} &= xy - \beta z
   \end{eqnarray}

and parameter values :math:`\sigma = 10,\,\rho = 28`, and :math:`\beta = 8/3`.

Exercise 1
----------

The script `mpecdt/src/data.py` integrates the Equation

.. math::
   
   \begin{equation}
   \dot{x} = \alpha x + \beta, \quad \alpha = 1, \beta = 0.1,
   \end{equation}
   
from initial condition :math:`x = 1.1` at time :math:`t=0` until
:math:`T=1`.

.. container:: tasks  

   Execute this script from the command line by changing to this
   directory and typing ::

     python data.py

This file is a module (which means that it can be imported and its
functions used within other code), but the code at the bottom will
execute if called in this way.

.. container:: tasks  

   Modify `mpecdt/src/data.py` so that the function `getdata()` returns a
   time series from the Lorenz equations above. Using the function plot3D
   which has been provided in the file, plot a trajectory with the
   initial conditions

   .. math::
      x=-0.587,\, y=-0.563,\, z=16.870

   between :math:`t=0` and :math:`t=100`, in intervals of :math:`\Delta
   t=0.05`.

   Add the python code to execute this at the bottom of your file. Don't
   forget to commit these changes to your local repository and then push
   back up to your forked repository on Bitbucket.

Exercise 2
----------

We are now going to start trying to forecast this system. We will
assume that we have observations of the x-component of the solution at
time intervals of width :math:`\Delta t=0.05`, and will try to predict
the next value of the x component given previous observations. The
simplest thing that we can do is linear extrapolation. We predict 
:math:`x^{n+1}` according to the extrapolation formula

.. math::
   x^{n+1}_p = 2x^n - x^{n-1}

where the subscript p stands for prediction. This formula fits a straight
line through :math:`x^{n-1}` and :math:`x^n` and evaluates it at time
:math:`t^{n+1}`.

.. container:: tasks  

   Add a new function `predict_linear()` to the file, which takes the
   Lorenz trajectory as input and returns an array of predicted values as
   output. Plot the actual and predicted solution on the same axes using
   `pylab.plot()`. 

.. container:: tasks  

   Add the python code to execute this at the bottom of your file. Since
   we will add all of the output of this exercise to the bottom of the
   file, it will become tedious when testing to execute it. For now just
   comment out the previous lines, and then uncomment when you share the
   file.

.. container:: tasks  

   Quantify the error in your forecast using the root-mean-square error
   (RMSE) 

   .. math::
      \sqrt{\frac{1}{N} \sum_{n=1}^N
      |x^n-x^n_p|^2}.
   
   Write a function to compute this, and add code to execute it at the
   bottom of the file.

Exercise 3
----------

.. container:: tasks  

   Extend the predict_linear function so that it can predict an arbitrary
   number of steps into the future (provided as a function
   argument). Plot the predicted values when predicting two steps into
   the future. Compute the prediction error when predicting two steps
   into the future with the previous error. Add code to execute all of
   this to the bottom of the file.

Exercise 4
----------

.. container:: tasks  

   Maybe we can try to improve our predictive skill by making use of more
   previous data. Using `scipy.interpolate.lagrange()`, write a function
   `predict_lagrange()` that fits a degree p polynomial through the data
   points :math:`x^n,\,x^{n-1},\,x^{n-2},\,\ldots\,x^{n-p}`, and
   evaluates it at :math:`t^{n+1}` to obtain the prediction
   `x^{n+1}_p`. Plot your predictions against the true values, and
   quantify the error. Then modify the function to predict an arbitrary
   number of steps into the future (provided as a function
   argument). Plot the predicted values and compute the prediction error
   when predicting two steps into the future.

Exercise 5
----------


It is clear that we need to go beyond pure extrapolation. We will now
make a data-driven model by fitting coefficients on a training data
set of length :math:`N_T`. We will then test the model by predicting
from values from a test set.

Given a chosen set of coefficients :math:`a_l \in \mathbb{R}`,
:math:`l=0,\ldots,p`, we can obtain a prediction of :math:`x^{j+p+1}`
for :math:`0<j\leq N_T-p-1` by the formula 

.. math::
   x_p^{k_\ast + 1} = \sum_{l=0}^p a_l \,x^{k_\ast - l}.

The quality of the predictions is measured by the residuals

.. math::
   \begin{align} \nonumber
   r_j &= x^{j+p+1}  - x_p^{j+p+1} \\
   &= x^{j+p+1} - \sum_{l=0}^{p} a_l
   x^{j+p-l} \end{align} 

for :math:`j=1,2,\ldots,J` with :math:`J=N_T -p-1`.  We now
seek the coefficients :math:`a_l` such that the
resulting time averaged RMSE is minimised over the training set. This
is equivalent to minimising the functional

.. math::
   L(\{a_l\}) = \frac{1}{2} \sum_{j=1}^{J} r_j^2;

we have recovered the method of least squares. The minimum of
:math:`L(\{a_l\})` is attained when the partial derivatives of :math:`L` with
respect to the coefficients :math:`a_l` vanish, \emph{i.e.},

.. math::
   \frac{\partial L}{\partial a_l} = -\sum_{j=1}^{J} x^{j+p-l} \,r_j = 0

for :math:`l = 0,\ldots,p`. These conditions lead to :math:`p+1`
linear equations which may be solved for the :math:`p+1` unknown
coefficients :math:`a_l`.

.. container:: tasks  

   Write a Python function `prediction_training()` that takes in a
   training data set, and returns the coefficients :math:`a_l`. Write
   another Python function `prediction_poly(n)` to perform predictions
   of data n steps ahead, given observations as above.

.. container:: tasks  

   Select the first 2000 observations for the training set, and use them
   in a prediction cycle to forecast the next 2000 observations one and
   two steps ahead. Compare the results with linear extrapolation. Add
   the code to execute this to the bottom of the file.

Advanced Exercise
-----------------

.. container:: tasks  

   Given a sequence of k (say k=5) observations of x,
   :math:`(x^n,x^{n+1},\ldots, x^{n+k})` write a Python function to
   optimise the residual for predictions
   :math:`(\vec{x}^n_p,\vec{x}^{n+1}_p,\ldots, \vec{x}^{n+k}_p)`,
   :math:`\vec{x}=(x,y,z)^T`, that are constrained by the model
   i.e. :math:`(\vec{x}^{n+1}_p,\ldots, \vec{x}^{n+k}_p)` are implicit
   functions of :math:`\vec{x}^n_p`, obtained by solving the Lorenz
   equations with initial condition :math:`\vec{x}^n_p`. One way to do
   this would be to use the nonlinear least squares solver
   `scipy.optimize.leastsq`.  Evaluate the performance of your
   forecasting approach for one and two steps ahead.
