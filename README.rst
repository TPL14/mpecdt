This is the documentation for the computational coursework of the
`EPSRC Centre for Doctoral Training in Mathematics of Planet Earth <http://mpecdt.org>`_.

For more information, please see the `documentation page itself <http://mpecdt.bitbucket.org>`_.
